(define-package "evil-collection" "20220104.1329" "A set of keybindings for Evil mode"
  '((emacs "25.1")
    (evil "1.2.13")
    (annalist "1.0"))
  :commit "e6be41bed7b4399db116038c7f0bf2f484065b48" :authors
  '(("James Nguyen" . "james@jojojames.com"))
  :maintainer
  '("James Nguyen" . "james@jojojames.com")
  :keywords
  '("evil" "tools")
  :url "https://github.com/emacs-evil/evil-collection")
;; Local Variables:
;; no-byte-compile: t
;; End:
