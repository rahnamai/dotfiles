(define-package "posframe" "20220107.2350" "Pop a posframe (just a frame) at point"
  '((emacs "26.1"))
  :commit "8a76d75aa851a314e60a3c20eec81e7e6f952a13" :authors
  '(("Feng Shu" . "tumashu@163.com"))
  :maintainer
  '("Feng Shu" . "tumashu@163.com")
  :keywords
  '("convenience" "tooltip")
  :url "https://github.com/tumashu/posframe")
;; Local Variables:
;; no-byte-compile: t
;; End:
